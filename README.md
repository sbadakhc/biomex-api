# BiOMEX Application Programming Interface

## Build and deploy to localhost.

Builds the docker container and tags it into the local registry.

```
./gradlew clean buildDocker
```

Run the docker container.

```
docker run -it -p 8080:8080 $(docker images | grep biomex-api | awk '{print $3}')
```

Verify the biomex-ui is running on the localhost and browse to http://localhost/settings.
