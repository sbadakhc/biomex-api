# The
# Tags: optional
    
Feature: Testing the Asset View REST API
  The assetview aggregates data from a number of different entities and returns all information
  required to render the Asset Pane in a JSON formatted response. The assetview will customise the data returned
  based on the user id provided to it. For example users belonging to different participants and groups will see
  different assets. These variations will be described in the scenarios below.

Scenario: An API call from a client with an existing user to the AssetView endpoint
  Given a test user with name "partya" and userId of 2
  When the client issues a GET request to the endpoint "/v1/asset/assetview/"
  Then the response code is OK 200
  Then the response contains the following data:
    | Attribute | Value |
    | Participant Name | Participant A |
    | User Name | partya |
    | Asset Name 0 | HCV-GLUE |
    | Asset Description 0 | Hepatitis C Virus |
    | Asset Name 1 | HIV-GLUE |
    | Asset Description 1 | Human Immune Virus |
    | Asset Name 2 | HEV-GLUE |
    | Asset Description 2 | Human Influenza Virus |
    | Asset Name 3 | HCV-GLUE |
    | Asset Description 3 | Hepatitis C Virus |
    | Asset Name 4 | HIV-GLUE |
    | Asset Description 4 | Human Immune Virus |
    | Asset Name 5 | HEV-GLUE |
    | Asset Description 5 | Human Influenza Virus |
    | User Group | Asset Manager |

