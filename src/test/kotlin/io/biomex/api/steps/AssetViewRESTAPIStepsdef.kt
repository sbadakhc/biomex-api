package io.biomex.biomex.steps

import cucumber.api.DataTable
import cucumber.api.java8.En
import io.restassured.RestAssured.get
import org.hamcrest.Matchers.equalTo
import org.hamcrest.Matchers.hasItem
import org.slf4j.LoggerFactory

var lastInstance: AssetViewRESTAPIStepsdef? = null
val log = LoggerFactory.getLogger(AssetViewRESTAPIStepsdef::class.qualifiedName)

class AssetViewRESTAPIStepsdef : En {
    var userId: Long? = 0
    var username: String = ""
    var endPoint: String = ""

    init {
        Given("^a test user with name \"([^\"]*)\" and userId of (\\d+)$") { name: String, id: Long ->
            username = name
            userId = id
            log.info("user name=$username user_id=$userId")
        }

        When("^the client issues a GET request to the endpoint \"([^\"]*)\"$") { endpoint: String ->
            // Write code here that turns the phrase above into concrete actions
            endPoint = endpoint
            log.info("endpoint = $endpoint")
        }

        Then("^the response code is OK (\\d+)$") { statusCode: Int ->
            // Write code here that turns the phrase above into concrete actions
            get("$endPoint/$userId").then().assertThat().statusCode(statusCode)
        }

        Then("^the response contains the following data:$") { attributeTable: DataTable ->
            // Write code here that turns the phrase above into concrete actions
            // For automatic transformation, change DataTable to one of
            // List<YourType>, List<List<E>>, List<Map<K,V>> or Map<K,V>.
            // E,K,V must be a scalar (String, Integer, Date, enum etc)
            val attributeMap = attributeTable.asMap(String::class.java, String::class.java)
            var response = get("$endPoint/$userId")
            response.then().body("participantDto.name", equalTo(attributeMap["Participant Name"]))
            response.then().body("participantDto.users.name", hasItem(attributeMap["User Name"]))
            response.then().body("participantDto.assets[0].name", equalTo(attributeMap["Asset Name 0"]))
            response.then().body("participantDto.assets[0].description", equalTo(attributeMap["Asset Description 0"]))
            response.then().body("participantDto.assets[1].name", equalTo(attributeMap["Asset Name 1"]))
            response.then().body("participantDto.assets[1].description", equalTo(attributeMap["Asset Description 1"]))
            response.then().body("participantDto.assets[2].name", equalTo(attributeMap["Asset Name 2"]))
            response.then().body("participantDto.assets[2].description", equalTo(attributeMap["Asset Description 2"]))
            response.then().body("participantDto.assets[3].name", equalTo(attributeMap["Asset Name 3"]))
            response.then().body("participantDto.assets[3].description", equalTo(attributeMap["Asset Description 3"]))
            response.then().body("participantDto.assets[4].name", equalTo(attributeMap["Asset Name 4"]))
            response.then().body("participantDto.assets[4].description", equalTo(attributeMap["Asset Description 4"]))
            response.then().body("participantDto.assets[5].name", equalTo(attributeMap["Asset Name 5"]))
            response.then().body("participantDto.assets[5].description", equalTo(attributeMap["Asset Description 5"]))
            response.then().body("participantDto.users[0].groups[0].name", equalTo(attributeMap["User Group"]))
        }
    }
}