package io.biomex.biomex.steps

import com.github.mkolisnyk.cucumber.runner.ExtendedCucumber
import com.github.mkolisnyk.cucumber.runner.ExtendedCucumberOptions
import cucumber.api.CucumberOptions
import cucumber.api.junit.Cucumber
import org.junit.runner.RunWith

@RunWith(ExtendedCucumber::class)
@ExtendedCucumberOptions(jsonReport = "build/test-results/cucumber/cucumber-report.json",
        retryCount = 3,
        detailedReport = true,
        detailedAggregatedReport = true,
        overviewReport = true,
        //coverageReport = true,
        jsonUsageReport = "build/test-results/cucumber/cucumber-report.json",
        usageReport = false,
        toPDF = true,
        excludeCoverageTags = ["@flaky"],
        includeCoverageTags = ["@passed"],
        outputFolder = "build/test-results/cucumber")
@CucumberOptions(
        plugin = ["pretty:build/test-results/cucumber/cucumber-pretty.txt", "html:build/test-results/cucumber/cucumber-html-report",
            "json:build/test-results/cucumber/cucumber-report.json", "usage:build/test-results/cucumber/cucumber-usage.json",
            "junit:build/test-results/cucumber/cucumber-results.xml"],
        glue = ["io.biomex.biomex.steps"],
        features = ["."])
class RunCukesTest


