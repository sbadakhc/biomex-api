package io.biomex.biomex

import io.biomex.biomex.model.AddressType
import io.biomex.biomex.model.ParticipantEntity
import io.biomex.biomex.model.ParticipantType
import io.biomex.biomex.repository.AddressRepository
import io.biomex.biomex.repository.ParticipantRepository
import io.biomex.biomex.service.AddressConfig
import io.biomex.biomex.service.AddressService
import io.biomex.biomex.service.ParticipantService
import io.biomex.biomex.service.dto.CreateAddressDto
import io.biomex.biomex.service.dto.CreateParticipantDto
import io.biomex.biomex.service.dto.UpdateAddressDto
import org.assertj.core.api.Assertions
import org.assertj.core.api.JUnitSoftAssertions
import org.junit.Rule
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig
import java.time.LocalDateTime

@ActiveProfiles("test")
@ContextConfiguration(classes = [JpaAddressServiceTest.Config::class, AddressConfig::class])
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@SpringBootTest
@DisplayName("JPA Address Service Tests")
@SpringJUnitConfig
internal class JpaAddressServiceTest {

    class Config

    @Autowired
    lateinit var participantService: ParticipantService

    @Autowired
    lateinit var participantRepo: ParticipantRepository

    @Autowired
    lateinit var addressRepo: AddressRepository

    @Autowired
    lateinit var service: AddressService

    @get:Rule
    var softly = JUnitSoftAssertions()

    var firstParticipantId: Long = 0
    var secondParticipantId: Long = 0
    var firstAddressId: Long = 0
    @BeforeAll
    fun setUp() {
        participantRepo.deleteAll()
        val (id1) = participantService.addParticipant(CreateParticipantDto(
                "participant1",
                "description1",
                ParticipantType.PRIVATE_LIMITED_COMPANY)
        )
        firstParticipantId = id1

        val (id2) = participantService.addParticipant(CreateParticipantDto(
                "participant2",
                "description2",
                ParticipantType.PRIVATE_LIMITED_COMPANY)
        )
        secondParticipantId = id2

        addressRepo.deleteAll()
        val (id3) = service.addAddress(
            CreateAddressDto( "line1", "line2", "line3", "city", "state",
            "country", "12345", firstParticipantId, AddressType.WORK
            )
        )
        firstAddressId = id3
    }

    @Test
    fun `'findByAddressId' should return null if Address Id doesn't exist`() {
        Assertions.assertThat(service.findByAddressId(0)).isNull()
    }

    @Test
    fun `'addAddress' should return created entity`() {
        val (id, line1, line2, line3,city, state, country, postcode, participant_id, address_type, updatedAt, createdAt)
                = service.addAddress(
            CreateAddressDto("line1", "line2", "line3", "city", "state",
            "country", "12345", secondParticipantId, AddressType.WORK )
        )
        softly.assertThat(line1).isEqualTo("line1")
        softly.assertThat(line2).isEqualTo("line2")
        softly.assertThat(line3).isEqualTo("line3")
        softly.assertThat(city).isEqualTo("city")
        softly.assertThat(state).isEqualTo("state")
        softly.assertThat(country).isEqualTo("country")
        softly.assertThat(postcode).isEqualTo("12345")
        softly.assertThat(participant_id).isEqualTo(secondParticipantId)
        softly.assertThat(address_type).isEqualTo(AddressType.WORK)
        softly.assertThat(updatedAt).isBeforeOrEqualTo(LocalDateTime.now())
        softly.assertThat(createdAt).isBeforeOrEqualTo(LocalDateTime.now())
    }

    @Test
    fun `'getByAddressId' should map existing entity from repository`() {
        val result = service.findByAddressId(firstAddressId)
        softly.assertThat(result?.line1).isEqualTo("line1")
        softly.assertThat(result?.line2).isEqualTo("line2")
        softly.assertThat(result?.line3).isEqualTo("line3")
        softly.assertThat(result?.city).isEqualTo("city")
        softly.assertThat(result?.state).isEqualTo("state")
        softly.assertThat(result?.country).isEqualTo("country")
        softly.assertThat(result?.postcode).isEqualTo("12345")
        softly.assertThat(result?.participant_id).isEqualTo(firstAddressId)
    }

    @Test
    fun `'findAllAddresses' should map entity from repository`() {
        val result = service.findAllAddresses()

        softly.assertThat(result).hasSize(1)
        result.forEach { softly.assertThat(it.id).isNotNull
        }
    }

    @Test
    fun `'updateAddress' should update existing values`() {
        val result = service.updateAddress(
            firstAddressId,
            UpdateAddressDto(
                "new line1",
                "new line2",
                "new line3",
                "new city",
                "new state",
                "new country",
                "12340",
                firstParticipantId,
                AddressType.WORK,
                LocalDateTime.now()
            )
        )
        softly.assertThat(result).isNotNull
        softly.assertThat(result?.id).isEqualTo(firstAddressId)
        softly.assertThat(result?.line1).isEqualTo("new line1")
        softly.assertThat(result?.line2).isEqualTo("new line2")
        softly.assertThat(result?.line2).isEqualTo("new line3")
        softly.assertThat(result?.city).isEqualTo("new city")
        softly.assertThat(result?.state).isEqualTo("new state")
        softly.assertThat(result?.country).isEqualTo("new country")
        softly.assertThat(result?.postcode).isEqualTo("12340")
        softly.assertThat(result?.updatedAt).isBeforeOrEqualTo(LocalDateTime.now())
        softly.assertThat(result?.participant_id).isEqualTo(firstParticipantId)
        softly.assertThat(result?.type).isEqualTo(AddressType.WORK)
        softly.assertThat(result?.createdAt).isBeforeOrEqualTo(LocalDateTime.now())
    }
}