package io.biomex.biomex

import io.biomex.biomex.model.ParticipantEntity
import io.biomex.biomex.model.ParticipantType
import io.biomex.biomex.repository.AssetRepository
import io.biomex.biomex.repository.ContractRepository
import io.biomex.biomex.repository.ParticipantRepository
import io.biomex.biomex.service.AssetConfig
import io.biomex.biomex.service.AssetService
import io.biomex.biomex.service.ContractService
import io.biomex.biomex.service.ParticipantService
import io.biomex.biomex.service.dto.CreateAssetDto
import io.biomex.biomex.service.dto.CreateContractDto
import io.biomex.biomex.service.dto.CreateParticipantDto
import io.biomex.biomex.service.dto.UpdateAssetDto
import org.assertj.core.api.Assertions.assertThat
import org.assertj.core.api.JUnitSoftAssertions
import org.junit.Rule
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.junit.jupiter.SpringExtension
import java.time.LocalDateTime

@ActiveProfiles("test")
@ContextConfiguration(classes = [JpaAssetServiceTest.Config::class, AssetConfig::class])
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@SpringBootTest
@DisplayName("JPA Asset Service Tests")
@ExtendWith(SpringExtension::class)
internal class JpaAssetServiceTest {

    class Config

    @Autowired
    lateinit var contractService: ContractService

    @Autowired
    lateinit var contractRepo: ContractRepository

    @Autowired
    lateinit var participantService: ParticipantService

    @Autowired
    lateinit var participantRepo: ParticipantRepository

    @Autowired
    lateinit var assetRepo: AssetRepository

    @Autowired
    lateinit var service: AssetService

    @get:Rule
    var softly = JUnitSoftAssertions()

    var firstParticipantId: Long = 0
    var secondParticipantId: Long = 0
    var firstContractId: Long = 0
    var secondContractId: Long = 0
    var firstAssetId: Long = 0
    @BeforeAll
    fun setUp() {
        participantRepo.deleteAll()
        val (id1) = participantService.addParticipant(CreateParticipantDto(
                "participant1",
                "description1",
                ParticipantType.PRIVATE_LIMITED_COMPANY)
        )
        firstParticipantId = id1

        val (id2) = participantService.addParticipant(CreateParticipantDto(
                "participant2",
                "description2",
                ParticipantType.PRIVATE_LIMITED_COMPANY)
        )
        secondParticipantId = id2

        /*
        val (id3) = contractService.addContract((CreateContractDto(
                type = "type1", price = "price1", participant_id = firstParticipantId, asset_id = 1)))
        firstContractId = id3

        val (id4) = contractService.addContract((CreateContractDto(
                type = "type2", price = "price2", participant_id = secondParticipantId, asset_id = 2)))
        secondContractId = id4
        */

        assetRepo.deleteAll()
        val (id5) = service.addAsset(
            CreateAssetDto(
                "asset1",
                "description1",
                firstParticipantId,
                1,
                "1.0.0",
                "www.biomex.io/asset/",
                "www.offpremise.com",
                "keywords1 keywords2 keywords3 keywords4"
            )
        )
        firstAssetId = id5
    }

    @Test
    fun `'findByAssetId' should return null if asset for asset Id doesn't exist`() {
        assertThat(service.findByAssetId(0)).isNull()
    }

    @Test
    fun `'addAsset' should return created entity`() {
        val (id, name, description, participant_id, contract_id, version, biomexEndPoint, offPremEndpoint, keywords, updatedAt, createdAt) = service.addAsset(
            CreateAssetDto(
                "asset2",
                "description2",
                secondParticipantId,
                1,
                "1.0.0",
                "www.biomex.io/asset/",
                "www.offpremise.com/",
                "keyword1 keyword2 keyword3 keyword4",
                LocalDateTime.now(),
                LocalDateTime.now()
            )
        )

        softly.assertThat(name).isEqualTo("asset2")
        softly.assertThat(description).isEqualTo("description2")
        softly.assertThat(participant_id).isEqualTo(secondParticipantId)
        softly.assertThat(contract_id).isEqualTo(1)
        softly.assertThat(version).isEqualTo("1.0.0")
        softly.assertThat(biomexEndPoint).isEqualTo("www.biomex.io/asset")
        softly.assertThat(offPremEndpoint).isEqualTo("www.offpremise.com")
        softly.assertThat(keywords).isEqualTo("keyword1 keyword2 keyword3 keyword4")
        softly.assertThat(updatedAt).isBeforeOrEqualTo(LocalDateTime.now())
        softly.assertThat(createdAt).isBeforeOrEqualTo(LocalDateTime.now())
    }

    @Test
    fun `'findByAssetId' should map existing entity from repository`() {
        val result = service.findByAssetId(firstAssetId)
        softly.assertThat(result?.name).isEqualTo("asset1")
        softly.assertThat(result?.description).isEqualTo("description1")
        softly.assertThat(result?.version).isEqualTo("1.0.0")
        softly.assertThat(result?.biomex_endpoint).isEqualTo("www.biomex.io/asset")
        softly.assertThat(result?.off_premise_endpoint).isEqualTo("www.offpremise.com")
        softly.assertThat(result?.keywords).isEqualTo("keyword1 keyword2 keyword3 keyword4")
        softly.assertThat(result?.participant_id).isEqualTo(firstParticipantId)
        softly.assertThat(result?.contract_id).isEqualTo(1)
    }

    @Test
    fun `'findAllAssets' should map entity from repository`() {
        val result = service.findAllAssets()

        softly.assertThat(result).hasSize(2)
        result.forEach {
            softly.assertThat(it.id).isNotNull
        }
    }

    @Test
    fun `'updateAsset' should update existing values`() {
        val result = service.updateAsset(
            firstAssetId, UpdateAssetDto(
                "new name", "new description",
                 secondParticipantId, 2, "1.0.0.", "www.biomex.io/new", "www.new.com/asset",
                "keyword1", LocalDateTime.now()
            )
        )
        softly.assertThat(result).isNotNull
        softly.assertThat(result?.id).isEqualTo(firstAssetId)
        softly.assertThat(result?.name).isEqualTo("new name")
        softly.assertThat(result?.description).isEqualTo("new description")
        softly.assertThat(result?.participant_id).isEqualTo(secondParticipantId)
        softly.assertThat(result?.contract_id).isEqualTo(2)
        softly.assertThat(result?.version).isEqualTo("1.0.0")
        softly.assertThat(result?.biomex_endpoint).isEqualTo("www.biomex.io/new")
        softly.assertThat(result?.off_premise_endpoint).isEqualTo("www.new.com/asset")
        softly.assertThat(result?.keywords).isEqualTo("keyword1")
        softly.assertThat(result?.updatedAt).isBeforeOrEqualTo(LocalDateTime.now())
        softly.assertThat(result?.createdAt).isBeforeOrEqualTo(LocalDateTime.now())
    }
}
