package io.biomex.biomex

import io.biomex.biomex.model.ParticipantEntity
import io.biomex.biomex.model.ParticipantType
import io.biomex.biomex.repository.AccountRepository
import io.biomex.biomex.repository.ParticipantRepository
import io.biomex.biomex.service.AccountConfig
import io.biomex.biomex.service.AccountService
import io.biomex.biomex.service.ParticipantService
import io.biomex.biomex.service.dto.CreateAccountDto
import io.biomex.biomex.service.dto.CreateParticipantDto
import io.biomex.biomex.service.dto.UpdateAccountDto
import org.assertj.core.api.Assertions.assertThat
import org.assertj.core.api.JUnitSoftAssertions
import org.junit.Rule
import org.junit.jupiter.api.*
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.junit.jupiter.SpringExtension
import java.time.LocalDateTime

@ActiveProfiles("test")
@ContextConfiguration(classes = [(JpaAccountServiceTest.Config::class), (AccountConfig::class)])
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@SpringBootTest
@DisplayName("JPA Account Service Tests")
@ExtendWith(SpringExtension::class)
internal class JpaAccountServiceTest {

    class Config

    @Autowired
    lateinit var participantRepo: ParticipantRepository

    @Autowired
    lateinit var participantService: ParticipantService

    @Autowired
    lateinit var accountRepo: AccountRepository

    @Autowired
    lateinit var service: AccountService

    @get:Rule
    var softly = JUnitSoftAssertions()

    var firstParticipantId: Long = 0
    var secondParticipantId: Long = 0
    var firstAccountId: Long = 0
    @BeforeAll
    fun setUp() {
        participantRepo.deleteAll()
        participantRepo.deleteAll()
        val (id1) = participantService.addParticipant(CreateParticipantDto(
                "participant1",
                "description1",
                ParticipantType.PRIVATE_LIMITED_COMPANY)
        )
        firstParticipantId = id1

        val (id2) = participantService.addParticipant(CreateParticipantDto(
                "participant2",
                "description2",
                ParticipantType.PRIVATE_LIMITED_COMPANY)
        )
        secondParticipantId = id2

        accountRepo.deleteAll()
        val (accountId) = service.addAccount(
            CreateAccountDto(
                "account1",
                "description1", "BIC", "IBAN", firstParticipantId
            )
        )
        firstAccountId = accountId
    }

    @AfterAll
    fun tearDown() {
        participantRepo.deleteAll()
        accountRepo.deleteAll()
    }

    @Test
    fun `'findByAccountId' should return null if Account Id doesn't exist`() {
        assertThat(service.findByAccountId(0)).isNull()
    }

    @Test
    fun `'addAccount' should return created entity`() {
        val (id, name, bic, iban, description, participant_id, updatedAt, createdAt) = service.addAccount(
            CreateAccountDto(
                "account2",
                "description2", "BIC", "IBAN", secondParticipantId
            )
        )
        softly.assertThat(name).isEqualTo("account2")
        softly.assertThat(bic).isEqualTo("BIC")
        softly.assertThat(iban).isEqualTo("IBAN")
        softly.assertThat(description).isEqualTo("description2")
        softly.assertThat(participant_id).isEqualTo(secondParticipantId)
        softly.assertThat(updatedAt).isBeforeOrEqualTo(LocalDateTime.now())
        softly.assertThat(createdAt).isBeforeOrEqualTo(LocalDateTime.now())
    }

    @Test
    fun `'findByAccountId' should map existing entity from repository`() {
        val result = service.findByAccountId(firstAccountId)
        softly.assertThat(result?.id).isEqualTo(firstAccountId)
        softly.assertThat(result?.name).isEqualTo("name1")
        softly.assertThat(result?.bic).isEqualTo("BIC")
        softly.assertThat(result?.iban).isEqualTo("IBAN")
        softly.assertThat(result?.description).isEqualTo("description1")
    }

    @Test
    fun `'findAllAccounts' should map entity from repository`() {
        val result = service.findAllAccounts()

        softly.assertThat(result).hasSize(2)
        result.forEach {
            softly.assertThat(it.id).isNotNull
        }
    }

    @Test
    fun `'updateAccount' should update existing values`() {
        val result = service.updateAccount(
            firstAccountId,
            UpdateAccountDto("new name", "new description", "BIC", "IBAN", firstParticipantId, LocalDateTime.now())
        )
        softly.assertThat(result).isNotNull
        softly.assertThat(result?.id).isEqualTo(firstAccountId)
        softly.assertThat(result?.name).isEqualTo("new name")
        softly.assertThat(result?.description).isEqualTo("new description")
        softly.assertThat(result?.bic).isEqualTo("BIC")
        softly.assertThat(result?.iban).isEqualTo("IBAN")
        softly.assertThat(result?.participant_id).isEqualTo(firstAccountId)
        softly.assertThat(result?.updatedAt).isBeforeOrEqualTo(LocalDateTime.now())
        softly.assertThat(result?.createdAt).isBeforeOrEqualTo(LocalDateTime.now())
    }
}