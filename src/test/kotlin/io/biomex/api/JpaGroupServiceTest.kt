package io.biomex.biomex

import io.biomex.biomex.repository.GroupRepository
import io.biomex.biomex.service.GroupConfig
import io.biomex.biomex.service.GroupService
import io.biomex.biomex.service.dto.CreateGroupDto
import io.biomex.biomex.service.dto.UpdateGroupDto
import org.assertj.core.api.Assertions.assertThat
import org.assertj.core.api.JUnitSoftAssertions
import org.junit.Rule
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.junit.jupiter.SpringExtension
import java.time.LocalDateTime
import javax.transaction.Transactional

@ActiveProfiles("test")
@ContextConfiguration(classes = [JpaGroupServiceTest.Config::class, GroupConfig::class])
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@SpringBootTest
@DisplayName("JPA Group Service Tests")
@ExtendWith(SpringExtension::class)
internal class JpaGroupServiceTest {

    class Config

    @Autowired
    lateinit var groupRepo: GroupRepository

    @Autowired
    lateinit var service: GroupService

    @get:Rule
    var softly = JUnitSoftAssertions()

    @BeforeAll
    fun setUp() {
        groupRepo.deleteAll()
        service.addGroup(CreateGroupDto("group1", "description1"))
    }

    @Test
    fun `'findByGroupId' should return null if Group for Group Id doesn't exist`() {
        assertThat(service.findByGroupId(0)).isNull()
    }

    @Test
    @Transactional
    fun `'addGroup' should return created entity`() {
        val (id, name, description, updatedAt, createdAt) = service.addGroup(
            CreateGroupDto(
                "group2", "description2",
                LocalDateTime.now(), LocalDateTime.now()
            )
        )
        softly.assertThat(id).isEqualTo(2)
        softly.assertThat(name).isEqualTo("group2")
        softly.assertThat(description).isEqualTo("description2")
        softly.assertThat(updatedAt).isBeforeOrEqualTo(LocalDateTime.now())
        softly.assertThat(createdAt).isBeforeOrEqualTo(LocalDateTime.now())
    }

    @Test
    @Transactional
    fun `'findByGroupId' should map entity from repository`() {
        val result = service.findByGroupId(1)
        softly.assertThat(result?.id).isEqualTo(1)
        softly.assertThat(result?.name).isEqualTo("group1")
        softly.assertThat(result?.description).isEqualTo("description1")
    }

    @Test
    fun `'findAllGroups' should map entity from repository`() {
        val result = service.findAllGroups()
        softly.assertThat(result).hasSize(1)
        result.forEach {
            softly.assertThat(it.id).isNotNull
        }
    }

    @Test
    fun `'updateGroup' should update existing values`() {
        val result = service.updateGroup(1, UpdateGroupDto("new name", "new description", LocalDateTime.now()))
        softly.assertThat(result).isNotNull
        softly.assertThat(result?.id).isEqualTo(1)
        softly.assertThat(result?.name).isEqualTo("new name")
        softly.assertThat(result?.description).isEqualTo("new description")
        softly.assertThat(result?.updatedAt).isBeforeOrEqualTo(LocalDateTime.now())
        softly.assertThat(result?.createdAt).isBeforeOrEqualTo(LocalDateTime.now())
    }
}
