package io.biomex.biomex

import io.biomex.biomex.repository.SubscriptionRepository
import io.biomex.biomex.service.SubscriptionService
import io.biomex.biomex.service.dto.CreateSubscriptionDto
import io.biomex.biomex.service.dto.UpdateSubscriptionDto
import org.assertj.core.api.Assertions.assertThat
import org.assertj.core.api.JUnitSoftAssertions
import org.junit.Rule
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.junit.jupiter.SpringExtension
import java.time.LocalDateTime

@ActiveProfiles("test")
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@SpringBootTest
@DisplayName("JPA Subscription Service Tests")
@ExtendWith(SpringExtension::class)
internal class JpaSubscriptionServiceTest {

    class Config

    @Autowired
    lateinit var subscriptionRepo: SubscriptionRepository

    @Autowired
    lateinit var service: SubscriptionService

    @get:Rule
    var softly = JUnitSoftAssertions()

    // for convenience
    val stubDate = LocalDateTime.now()

    @BeforeAll
    fun setUp() {
        subscriptionRepo.deleteAll()
        service.addSubscription(
            CreateSubscriptionDto(
                    1111,
                    11.11F,
                    "GBP",
                    "1 YEAR",
                    LocalDateTime.now(),
                    LocalDateTime.now()
            )
        )
    }

    @Test
    fun `'findBySubscriptionId' should return null if Subscription Id doesn't exist`() {
        assertThat(service.findBySubscriptionId(0)).isNull()
    }

    @Test
    fun `'addSubscription' should return created entity`() {
        val (contractId, cost, currency, term, startDate, expiryDate, createdAt, updatedAt) = service.addSubscription(
            CreateSubscriptionDto(
                    2222,
                    22.22F,
                    "USD",
                    "2 YEARS",
                    stubDate,
                    stubDate
            )
        )
        softly.assertThat(contractId).isEqualTo(2222)
        softly.assertThat(cost).isEqualTo(22.22F)
        softly.assertThat(currency).isEqualTo("USD")
        softly.assertThat(term).isEqualTo("2 YEARS")
        softly.assertThat(startDate).isEqualTo(stubDate)
        softly.assertThat(expiryDate).isEqualTo(stubDate)
        softly.assertThat(updatedAt).isBeforeOrEqualTo(LocalDateTime.now())
        softly.assertThat(createdAt).isBeforeOrEqualTo(LocalDateTime.now())
    }


    @Test
    fun `'findBySubscriptionId' should map existing entity from repository`() {
        val result = service.findBySubscriptionId(1)
        softly.assertThat(result?.id).isEqualTo(1)
        softly.assertThat(result?.contractId).isEqualTo(1111)
        softly.assertThat(result?.cost).isEqualTo(11.11F)
        softly.assertThat(result?.term).isEqualTo("1 YEAR")
        softly.assertThat(result?.startDate).isEqualTo(stubDate)
        softly.assertThat(result?.expiryDate).isEqualTo(stubDate)
        softly.assertThat(result?.updatedAt).isBeforeOrEqualTo(LocalDateTime.now())
        softly.assertThat(result?.createdAt).isBeforeOrEqualTo(LocalDateTime.now())
    }

    @Test
    fun `'findAllSubscriptions' should map entity from repository`() {
        val result = service.findAllSubscriptions()

        softly.assertThat(result).hasSize(2)
        result.forEach {
            softly.assertThat(it.id).isNotNull
        }
    }

    @Test
    fun `'updateSubscription' should update existing values`() {
        val result = service.updateSubscription(
            UpdateSubscriptionDto(3333, 33.33F, "JPY", "3 YEARS", stubDate, stubDate), 1
        )
        softly.assertThat(result).isNotNull
        softly.assertThat(result?.id).isEqualTo(1)
        softly.assertThat(result?.contractId).isEqualTo(3333)
        softly.assertThat(result?.cost).isEqualTo(33.33F)
        softly.assertThat(result?.currency).isEqualTo("JPY")
        softly.assertThat(result?.term).isEqualTo("3 YEARS")
        softly.assertThat(result?.startDate).isEqualTo(stubDate)
        softly.assertThat(result?.expiryDate).isEqualTo(stubDate)
        softly.assertThat(result?.updatedAt).isBeforeOrEqualTo(LocalDateTime.now())
        softly.assertThat(result?.createdAt).isBeforeOrEqualTo(LocalDateTime.now())
    }
}