package io.biomex.biomex

import io.biomex.biomex.model.GroupEntity
import io.biomex.biomex.model.ParticipantEntity
import io.biomex.biomex.model.ParticipantType
import io.biomex.biomex.repository.ParticipantRepository
import io.biomex.biomex.repository.UserRepository
import io.biomex.biomex.service.ParticipantService
import io.biomex.biomex.service.UserConfig
import io.biomex.biomex.service.UserService
import io.biomex.biomex.service.dto.CreateParticipantDto
import io.biomex.biomex.service.dto.CreateUserDto
import io.biomex.biomex.service.dto.UpdateUserDto
import org.assertj.core.api.Assertions
import org.assertj.core.api.JUnitSoftAssertions
import org.junit.Rule
import org.junit.jupiter.api.*
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig
import java.time.LocalDateTime

@ActiveProfiles("test")
@ContextConfiguration(classes = [(JpaUserServiceTest.Config::class), (UserConfig::class)])
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@SpringBootTest
@DisplayName("JPA User Service Tests")
@SpringJUnitConfig
internal class JpaUserServiceTest {

    class Config

    @Autowired
    lateinit var userRepo: UserRepository

    @Autowired
    lateinit var participantRepo: ParticipantRepository

    @Autowired
    lateinit var participantService: ParticipantService

    @Autowired
    lateinit var service: UserService

    @get:Rule
    var softly = JUnitSoftAssertions()

    var firstParticipantId: Long = 0
    var firstUserId: Long = 0
    @BeforeAll
    fun setUp() {
        userRepo.deleteAll()
        participantRepo.deleteAll()
        val (participantId) = participantService.addParticipant(CreateParticipantDto(
                "participant1",
                "description1",
                ParticipantType.PRIVATE_LIMITED_COMPANY)
        )
        firstParticipantId = participantId

        val (id1) = service.addUser(CreateUserDto("user1", "password1", participantId))
        firstUserId = id1
    }

    @AfterAll()
    fun tearDown() {
        userRepo.deleteAll()
        participantRepo.deleteAll()
    }

    @Test
    fun `'findByUserId' should return null if User Id doesn't exist`() {
        Assertions.assertThat(service.findByUserId(0)).isNull()
    }

    var secondUserId: Long = 0
    @Test
    fun `'addUser' should return created entity`() {
        val (participantId) = participantService.addParticipant(CreateParticipantDto(
                "participant2",
                "description2",
                ParticipantType.PRIVATE_LIMITED_COMPANY)
        )
        val (id, name, password, groups, participant_id, updatedAt, createdAt) = service.addUser(
            CreateUserDto(
                "user2",
                "password2",
                 participantId
            )
        )
        secondUserId = id
        softly.assertThat(name).isEqualTo("user2")
        softly.assertThat(password).isEqualTo("password2")
        softly.assertThat(groups).isEqualTo(emptySet<GroupEntity>())
        softly.assertThat(participant_id).isEqualTo(participantId)
        softly.assertThat(updatedAt).isBeforeOrEqualTo(LocalDateTime.now())
        softly.assertThat(createdAt).isBeforeOrEqualTo(LocalDateTime.now())
    }

    @Test
    fun `'findByUserId' should map existing entity from repository`() {
        val result = service.findByUserId(firstUserId)
        softly.assertThat(result?.id).isEqualTo(firstUserId)
        softly.assertThat(result?.name).isEqualTo("user1")
        softly.assertThat(result?.password).isEqualTo("password1")
        softly.assertThat(result?.participant_id).isEqualTo(firstParticipantId)
    }

    @Test
    fun `'findAllUsers' should map entity from repository`() {
        val result = service.findAllUsers()

        softly.assertThat(result).hasSize(2)
        result.forEach {
            softly.assertThat(it.id).isNotNull
        }
    }

    @Test
    fun `'updateUser' should update existing values`() {
        val result = service.updateUser(firstUserId, UpdateUserDto("new name", "new password", firstParticipantId, LocalDateTime.now()))
        softly.assertThat(result).isNotNull
        softly.assertThat(result?.id).isEqualTo(firstUserId)
        softly.assertThat(result?.name).isEqualTo("new name")
        softly.assertThat(result?.password).isEqualTo("new password")
        softly.assertThat(result?.participant_id).isEqualTo(firstParticipantId)
        softly.assertThat(result?.updatedAt).isBeforeOrEqualTo(LocalDateTime.now())
        softly.assertThat(result?.createdAt).isBeforeOrEqualTo(LocalDateTime.now())
    }
}