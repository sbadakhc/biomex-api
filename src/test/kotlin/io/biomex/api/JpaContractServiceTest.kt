package io.biomex.biomex


import io.biomex.biomex.model.ParticipantEntity
import io.biomex.biomex.model.ParticipantType
import io.biomex.biomex.repository.ContractRepository
import io.biomex.biomex.repository.ParticipantRepository
import io.biomex.biomex.service.ContractConfig
import io.biomex.biomex.service.ContractService
import io.biomex.biomex.service.ParticipantService
import io.biomex.biomex.service.dto.CreateContractDto
import io.biomex.biomex.service.dto.CreateParticipantDto
import io.biomex.biomex.service.dto.UpdateContractDto
import org.assertj.core.api.Assertions.assertThat
import org.assertj.core.api.JUnitSoftAssertions
import org.junit.Rule
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.junit.jupiter.SpringExtension
import java.time.LocalDateTime

@ActiveProfiles("test")
@ContextConfiguration(classes = [JpaContractServiceTest.Config::class, ContractConfig::class])
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@SpringBootTest
@DisplayName("JPA Contract Service Tests")
@ExtendWith(SpringExtension::class)
internal class JpaContractServiceTest {

    class Config

    @Autowired
    lateinit var participantService: ParticipantService

    @Autowired
    lateinit var participantRepo: ParticipantRepository

    @Autowired
    lateinit var contractRepo: ContractRepository

    @Autowired
    lateinit var service: ContractService

    @get:Rule
    var softly = JUnitSoftAssertions()

    var firstParticipantId: Long = 0
    var secondParticipantId: Long = 0
    var firstContractId: Long = 0
    @BeforeAll
    fun setUp() {
        participantRepo.deleteAll()
        val (id1) = participantService.addParticipant(CreateParticipantDto(
                "participant1",
                "description1",
                ParticipantType.PRIVATE_LIMITED_COMPANY)
        )
        firstParticipantId = id1

        val (id2) = participantService.addParticipant(CreateParticipantDto(
                "participant2",
                "description2",
                ParticipantType.PRIVATE_LIMITED_COMPANY)
        )
        secondParticipantId = id2

        contractRepo.deleteAll()
        val (id3) = service.addContract(
            CreateContractDto(
                "public",
                "subscription",
                firstParticipantId,
                1
            )
        )
        firstContractId = id3
    }

    @Test
    fun `'findByContractId' should return null if contract for contract Id doesn't exist`() {
        assertThat(service.findByContractId(0)).isNull()
    }

    @Test
    fun `'addContract' should return created entity`() {
        val (id, type, price, participant_id, asset_id, updatedAt, createdAt) = service.addContract(
            CreateContractDto(
                "public",
                "subscription",
                firstParticipantId,
                1,
                LocalDateTime.now(),
                LocalDateTime.now()
            )
        )

        softly.assertThat(type).isEqualTo("public")
        softly.assertThat(price).isEqualTo("subscription")
        softly.assertThat(participant_id).isEqualTo(firstParticipantId)
        softly.assertThat(asset_id).isEqualTo(1)
        softly.assertThat(updatedAt).isBeforeOrEqualTo(LocalDateTime.now())
        softly.assertThat(createdAt).isBeforeOrEqualTo(LocalDateTime.now())
    }

    @Test
    fun `'findByContractId' should map existing entity from repository`() {
        val result = service.findByContractId(1)
        softly.assertThat(result?.id).isEqualTo(1)
        softly.assertThat(result?.type).isEqualTo("public")
        softly.assertThat(result?.price).isEqualTo("subscription")
        softly.assertThat(result?.participant_id).isEqualTo(firstParticipantId)
        softly.assertThat(result?.asset_id).isEqualTo(1)
    }

    @Test
    fun `'findAllContracts' should map entity from repository`() {
        val result = service.findAllContracts()

        softly.assertThat(result).hasSize(1)
        result.forEach {
            softly.assertThat(it.id).isNotNull
        }
    }

    @Test
    fun `'updateContract' should update existing values`() {
        val result = service.updateContract(1, UpdateContractDto("private", "on-demand", secondParticipantId, 2, LocalDateTime.now()))
        softly.assertThat(result).isNotNull
        softly.assertThat(result?.id).isEqualTo(1)
        softly.assertThat(result?.type).isEqualTo("private")
        softly.assertThat(result?.price).isEqualTo("on-demand")
        softly.assertThat(result?.participant_id).isEqualTo(secondParticipantId)
        softly.assertThat(result?.asset_id).isEqualTo(2)
        softly.assertThat(result?.updatedAt).isBeforeOrEqualTo(LocalDateTime.now())
        softly.assertThat(result?.createdAt).isBeforeOrEqualTo(LocalDateTime.now())
    }
}
