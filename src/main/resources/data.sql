/* Participant data */
INSERT INTO participant ( id, name, description, type, updated_at, created_at ) VALUES
 ( 1, 'BiOMEX Limited', 'The Biomedical Algorithmic Exchange', 7, '2017-12-06T01:00:00.000', '2017-12-06T01:00:00.000');

INSERT INTO participant ( id, name, description, type, updated_at, created_at ) VALUES
 ( 2, 'Participant A', 'A Digital Biomedical Asset Producer', 8, '2017-12-06T02:00:00.000', '2017-12-06T02:00:00.000');

INSERT INTO participant ( id, name, description, type, updated_at, created_at ) VALUES
 ( 3, 'Participant B', 'A Digital Biomedical Asset Consumer', 0, '2017-12-06T03:00:00.000', '2017-12-06T03:00:00.000');

INSERT INTO participant ( id, name, description, type, updated_at, created_at ) VALUES
 ( 4, 'The University of Glasgow', 'The University Court Of The University Of Glasgow', 11, '2017-12-06T04:00:00.000', '2017-12-06T04:00:00.000');

INSERT INTO participant ( id, name, description, type, updated_at, created_at ) VALUES
 ( 5, 'The University of Exeter', 'The Research University of Exeter', 11, '2017-12-06T05:00:00.000', '2017-12-06T05:00:00.000');

INSERT INTO participant ( id, name, description, type, updated_at, created_at ) VALUES
 ( 6, 'The University of Oxford', 'The Research University of Oxford', 11, '2017-12-06T06:00:00.000', '2017-12-06T06:00:00.000');

INSERT INTO participant ( id, name, description, type, updated_at, created_at ) VALUES
 ( 7, 'The University of Cambridge', 'The Research University of Cambridge', 11, '2017-12-06T07:00:00.000', '2017-12-06T07:00:00.000');

INSERT INTO participant ( id, name, description, type, updated_at, created_at ) VALUES
 ( 8, 'Columbia University', 'Private Ivy League Research University', 12, '2017-12-06T08:00:00.000', '2017-12-06T08:00:00.000');

INSERT INTO participant ( id, name, description, type, updated_at, created_at ) VALUES
 ( 9, 'Oxford Nanopore Technologies', 'Oxford Nanopore Technologies Ltd is a private company headquartered at the Oxford Science Park', 6, '2017-12-06T09:00:00.000', '2017-12-06T09:00:00.000');

/* Account Data */
INSERT INTO account ( id, name, bic, iban, description, participant_id, updated_at, created_at ) VALUES
 ( 1, 'Biomedical Algorithmic Exchange Limited', 'Bank account for BiOMEX Limited', 'BIOMGB00AAA', 'GB29NWBK1111111111', 1, '2017-12-06T01:00:00.000', '2017-12-06T01:00:00.000');

INSERT INTO account ( id, name, bic, iban, description, participant_id, updated_at, created_at ) VALUES
 ( 2, 'Participant A', 'Bank account for Participant A', 'PAAAGB00AAA', 'GB29NWBK2222222222', 2, '2017-12-06T02:00:00.000', '2017-12-06T02:00:00.000');

INSERT INTO account ( id, name, bic, iban, description, participant_id, updated_at, created_at ) VALUES
 ( 3, 'Participant B', 'Bank account for Participant B', 'PBBBGB00BBB', 'GB29NWBK3333333333', 3, '2017-12-06T03:00:00.000', '2017-12-06T03:00:00.000');

INSERT INTO account ( id, name, bic, iban, description, participant_id, updated_at, created_at ) VALUES
 ( 4, 'The University of Glasgow', 'Bank account for the University of Glasgow', 'UNGLGB00BBB', 'GB24NWBK4444444444', 4, '2017-12-06T04:00:00.000', '2017-12-06T04:00:00.000');

INSERT INTO account ( id, name, bic, iban, description, participant_id, updated_at, created_at ) VALUES
 ( 5, 'The University of Exeter', 'Bank account for the University of Exeter', 'UNEXGB00BBB', 'GB25NWBK5555555555', 5, '2017-12-06T05:00:00.000', '2017-12-06T05:00:00.000');

INSERT INTO account ( id, name, bic, iban, description, participant_id, updated_at, created_at ) VALUES
 ( 6, 'The University of Oxford', 'Bank account for the University of Oxford', 'UNOXGB00BBB', 'GB26NWBK6666666666', 6, '2017-12-06T06:00:00.000', '2017-12-05T06:00:00.000');

INSERT INTO account ( id, name, bic, iban, description, participant_id, updated_at, created_at ) VALUES
 ( 7, 'The University of Cambridge', 'Bank account for the University of Cambridge', 'UNCBGB00BBB', 'GB26NWBK7777777777', 7, '2017-12-06T07:00:00.000', '2017-12-05T07:00:00.000');

INSERT INTO account ( id, name, bic, iban, description, participant_id, updated_at, created_at ) VALUES
 ( 8, 'Comumbia University', 'Bank account for the University of Comlumbia', 'UNCLUS00AAA', 'US26NWBK8888888888', 8, '2017-12-06T08:00:00.000', '2017-12-05T08:00:00.000');

INSERT INTO account ( id, name, bic, iban, description, participant_id, updated_at, created_at ) VALUES
 ( 9, 'Oxford Nanopore Technologies', 'Bank account for the Oxford Nanopore Technologies', 'UNOX00OOO', 'UK26NWBK9999999999', 9, '2017-12-06T09:00:00.000', '2017-12-05T09:00:00.000');

/* Address Data */
INSERT INTO address ( id, line1, line2, line3, city, state, country, postcode, participant_id, type, updated_at, created_at ) VALUES
 ( 1, 'First Floor', '85 Great Portland Street', '', 'London', 'Westminster', 'United Kingdom', 'W1W7LT', 1, 3,
  '2017-12-06T01:00:00.000', '2017-12-06T01:00:00.000');

INSERT INTO address ( id, line1, line2, line3, city, state, country, postcode, participant_id, type, updated_at, created_at ) VALUES
 ( 2, 'Second Floor', '10 Old Broad Street', '', 'London', 'City of London', 'United Kingdom', 'EC1 2DA', 2, 3,
  '2017-12-06T02:00:00.000', '2017-12-06T02:00:00.000');

INSERT INTO address ( id, line1, line2, line3, city, state, country, postcode, participant_id, type, updated_at, created_at ) VALUES
 ( 3, 'Third Floor', '6/8 Bishopsgate', '', 'London', 'City of London', 'United Kingdom', 'EC2 4DN', 3, 3,
  '2017-12-06T03:00:00.000', '2017-12-06T03:00:00.000');

INSERT INTO address ( id, line1, line2, line3, city, state, country, postcode, participant_id, type, updated_at, created_at ) VALUES
 ( 4, 'University Avenue', '', '', 'Glasgow', 'Scotland', 'United Kingdom', 'G12 8QQ', 4, 3,
  '2017-12-06T04:00:00.000', '2017-12-06T04:00:00.000');

INSERT INTO address ( id, line1, line2, line3, city, state, country, postcode, participant_id, type, updated_at, created_at ) VALUES
 ( 5, 'Mail Room', 'The Old Library', 'Prince of Wales Road', 'Exeter', 'Deveon', 'United Kingdom', 'EX4 4SB', 5, 3,
  '2017-12-06T05:00:00.000', '2017-12-06T05:00:00.000');

INSERT INTO address ( id, line1, line2, line3, city, state, country, postcode, participant_id, type, updated_at, created_at ) VALUES
 ( 6, 'University Offices', 'Wellington Square', '', 'Oxford', 'Oxfordshire', 'United Kingdom', 'OX1 2JD', 6, 3,
  '2017-12-06T06:00:00.000', '2017-12-06T06:00:00.000');

INSERT INTO address ( id, line1, line2, line3, city, state, country, postcode, participant_id, type, updated_at, created_at ) VALUES
 ( 7, 'The Old Schools', 'Trinity Ln', '', 'Cambridge', 'Cambridgeshire', 'United Kingdom', 'CB2 1TN', 7, 3,
  '2017-12-06T07:00:00.000', '2017-12-06T07:00:00.000');

INSERT INTO address ( id, line1, line2, line3, city, state, country, postcode, participant_id, type, updated_at, created_at ) VALUES
 ( 8, '116th St & Broadway', '', 'Manhaton', 'New York', 'NY', 'United States Of America', '10027', 8, 3,
  '2017-12-06T08:00:00.000', '2017-12-06T08:00:00.000');

INSERT INTO address ( id, line1, line2, line3, city, state, country, postcode, participant_id, type, updated_at, created_at ) VALUES
 ( 9, 'Edmund Cartwright House', '4 Robert Robinson Avenue', 'Oxford Science Park', 'Oxford', 'Oxfordshire', 'United Kingdom', 'OX4 4GA', 9, 3,
  '2017-12-06T09:00:00.000', '2017-12-06T09:00:00.000');

/* Contract Data */
INSERT INTO contract ( id, type, price, participant_id, asset_id, updated_at, created_at ) VALUES
 ( 1, 'Public', 'Subscription', 2, 1, '2017-12-06T01:00:00.000', '2017-12-06T01:00:00.000');

INSERT INTO contract ( id, type, price, participant_id, asset_id, updated_at, created_at ) VALUES
 ( 2, 'Public', 'Subscription', 2, 2, '2017-12-06T01:00:00.000', '2017-12-06T01:00:00.000');

INSERT INTO contract ( id, type, price, participant_id, asset_id, updated_at, created_at ) VALUES
 ( 3, 'Public', 'Subscription', 2, 3, '2017-12-06T01:00:00.000', '2017-12-06T01:00:00.000');

INSERT INTO contract ( id, type, price, participant_id, asset_id, updated_at, created_at ) VALUES
 ( 4, 'Public', 'On-demand', 2, 4, '2017-12-06T01:00:00.000', '2017-12-06T01:00:00.000');

INSERT INTO contract ( id, type, price, participant_id, asset_id, updated_at, created_at ) VALUES
 ( 5, 'Public', 'Bid/Ask', 2, 5, '2017-12-06T01:00:00.000', '2017-12-06T01:00:00.000');

INSERT INTO contract ( id, type, price, participant_id, asset_id, updated_at, created_at ) VALUES
 ( 6, 'Private', 'Subscription', 2, 6, '2017-12-06T01:00:00.000', '2017-12-06T01:00:00.000');

INSERT INTO contract ( id, type, price, participant_id, asset_id, updated_at, created_at ) VALUES
 ( 7, 'Private', 'On-demand', 2, 7, '2017-12-06T01:00:00.000', '2017-12-06T01:00:00.000');

INSERT INTO contract ( id, type, price, participant_id, asset_id, updated_at, created_at ) VALUES
 ( 8, 'Private', 'Bid/Ask', 2, 8, '2017-12-06T01:00:00.000', '2017-12-06T01:00:00.000');

INSERT INTO contract ( id, type, price, participant_id, asset_id, updated_at, created_at ) VALUES
 ( 9, 'Public', 'Subscription', 2, 8, '2017-12-06T01:00:00.000', '2017-12-06T01:00:00.000');

/* User Profile data */
INSERT INTO user_profile ( id, first_name, last_name, other_name, nationality, updated_at, created_at ) VALUES
 ( 1, 'Avicenna', 'Ibn', 'Sina', 'Iranian', '2017-12-06T01:00:00.000', '2017-12-06T01:00:00.000', );

INSERT INTO user_profile ( id, first_name, last_name, other_name, nationality, updated_at, created_at ) VALUES
 ( 2, 'Charles', 'Darwin', 'Robert', 'British', '2017-12-06T02:00:00.000', '2017-12-06T02:00:00.000', );

INSERT INTO user_profile ( id, first_name, last_name, other_name, nationality, updated_at, created_at ) VALUES
 ( 3, 'Francis', 'Crick', '', 'British', '2017-12-06T03:00:00.000', '2017-12-06T03:00:00.000', );

INSERT INTO user_profile ( id, first_name, last_name, other_name, nationality, updated_at, created_at ) VALUES
 ( 4, 'Adam', 'Smith', '', 'British', '2017-12-06T04:00:00.000', '2017-12-06T04:00:00.000', );

INSERT INTO user_profile ( id, first_name, last_name, other_name, nationality, updated_at, created_at ) VALUES
 ( 5, 'Charles', 'Babbage', '', 'British', '2017-12-06T05:00:00.000', '2017-12-06T05:00:00.000', );

INSERT INTO user_profile ( id, first_name, last_name, other_name, nationality, updated_at, created_at ) VALUES
 ( 6, 'Mary', 'Anning', '', 'British', '2017-12-06T06:00:00.000', '2017-12-06T06:00:00.000', );

INSERT INTO user_profile ( id, first_name, last_name, other_name, nationality, updated_at, created_at ) VALUES
 ( 7, 'Alan', 'Turing', '', 'British', '2017-12-06T07:00:00.000', '2017-12-06T07:00:00.000', );

INSERT INTO user_profile ( id, first_name, last_name, other_name, nationality, updated_at, created_at ) VALUES
 ( 8, 'Isaac', 'Asimov', '', 'Russian', '2017-12-06T08:00:00.000', '2017-12-06T08:00:00.000', );

INSERT INTO user_profile ( id, first_name, last_name, other_name, nationality, updated_at, created_at ) VALUES
 ( 9, 'John', 'Tolkien', 'Ronald Reuel', 'Russian', '2017-12-06T09:00:00.000', '2017-12-06T09:00:00.000', );

/* Users data */
INSERT INTO users ( id, name, password, participant_id, updated_at, created_at , user_profile_id) VALUES
 ( 1, 'biomex', 'usrbiomex001', 1, '2017-12-06T01:00:00.000', '2017-12-06T01:00:00.000', 9);

INSERT INTO users ( id, name, password, participant_id, updated_at, created_at, user_profile_id ) VALUES
 ( 2, 'partya', 'usrpartya002', 2, '2017-12-06T02:00:00.000', '2017-12-06T02:00:00.000', 8);

INSERT INTO users ( id, name, password, participant_id, updated_at, created_at, user_profile_id ) VALUES
 ( 3, 'partyb', 'usrpartyb003', 3, '2017-12-06T03:00:00.000', '2017-12-06T03:00:00.000', 7);

INSERT INTO users ( id, name, password, participant_id, updated_at, created_at, user_profile_id ) VALUES
 ( 4, 'glasgow', 'usrglasgow', 4, '2017-12-06T04:00:00.000', '2017-12-06T04:00:00.000', 6);

INSERT INTO users ( id, name, password, participant_id, updated_at, created_at, user_profile_id ) VALUES
 ( 5, 'exeter', 'usrexeter', 5, '2017-12-06T05:00:00.000', '2017-12-06T05:00:00.000', 5);

INSERT INTO users ( id, name, password, participant_id, updated_at, created_at, user_profile_id ) VALUES
 ( 6, 'oxford', 'usroxford', 6, '2017-12-06T06:00:00.000', '2017-12-06T06:00:00.000', 4);

INSERT INTO users ( id, name, password, participant_id, updated_at, created_at, user_profile_id ) VALUES
 ( 7, 'cambridge', 'usrcambridge', 7, '2017-12-06T07:00:00.000', '2017-12-06T07:00:00.000', 3);

INSERT INTO users ( id, name, password, participant_id, updated_at, created_at, user_profile_id ) VALUES
 ( 8, 'columbia', 'usrcolumbia', 8,'2017-12-06T08:00:00.000', '2017-12-06T08:00:00.000', 2);

INSERT INTO users ( id, name, password, participant_id, updated_at, created_at, user_profile_id ) VALUES
 ( 9, 'nanopore', 'usrnanopore', 9, '2017-12-06T09:00:00.000', '2017-12-06T09:00:00.000', 1);

/* Asset Data */
INSERT INTO asset ( id, name, description, participant_id, contract_id, version, biomex_endpoint, off_premise_endpoint, keywords, updated_at, created_at )
 VALUES ( 1, 'HCV-GLUE', 'Hepatitis C Virus', 2, 1, '1.0.0', 'www.biomex.io/asset/glue/hcv/v100', 'www.offpremise.com/', 'glue algorithm DNA RNA virus human hepatitis', '2017-12-06T01:00:00.000', '2017-12-06T01:00:00.000');

INSERT INTO asset ( id, name, description, participant_id, contract_id, version, biomex_endpoint, off_premise_endpoint, keywords, updated_at, created_at )
 VALUES ( 2, 'HIV-GLUE', 'Human Immune Virus', 2, 2, '1.0.0',  'www.biomex.io/asset/glue/hiv/v100', 'www.offpremise.com/', 'glue algorithm DNA RNA virus human AIDS HIV', '2017-12-06T01:00:00.000', '2017-12-06T01:00:00.000');

INSERT INTO asset ( id, name, description, participant_id, contract_id, version, biomex_endpoint, off_premise_endpoint, keywords, updated_at, created_at )
 VALUES ( 3, 'HEV-GLUE', 'Human Influenza Virus', 2, 3, '1.0.0', 'www.biomex.io/asset/glue/hev/v100', 'www.offpremise.com/', 'glue algorithm DNA RNA virus human flu influenza', '2017-12-06T01:00:00.000', '2017-12-06T01:00:00.000');

INSERT INTO asset ( id, name, description, participant_id, contract_id, version, biomex_endpoint, off_premise_endpoint, keywords, updated_at, created_at )
 VALUES ( 4, 'HCV-GLUE', 'Hepatitis C Virus', 2, 4, '1.0.0', 'www.biomex.io/asset/glue/hcv/v100', 'www.offpremise.com/', 'glue algorithm DNA RNA virus human hepatitis', '2017-12-06T01:00:00.000', '2017-12-06T01:00:00.000');

INSERT INTO asset ( id, name, description, participant_id, contract_id, version, biomex_endpoint, off_premise_endpoint, keywords, updated_at, created_at )
 VALUES ( 5, 'HIV-GLUE', 'Human Immune Virus', 2, 5, '1.0.0', 'www.biomex.io/asset/glue/hiv/v100', 'www.offpremise.com/', 'glue algorithm DNA RNA virus human AIDS HIV', '2017-12-06T01:00:00.000', '2017-12-06T01:00:00.000');

INSERT INTO asset ( id, name, description, participant_id, contract_id, version, biomex_endpoint, off_premise_endpoint, keywords, updated_at, created_at )
 VALUES ( 6, 'HEV-GLUE', 'Human Influenza Virus', 2, 6, '1.0.0', 'www.biomex.io/asset/glue/hev/v100', 'www.offpremise.com/', 'glue algorithm DNA RNA virus human flu influenza', '2017-12-06T01:00:00.000', '2017-12-06T01:00:00.000');

INSERT INTO asset ( id, name, description, participant_id, contract_id, version, biomex_endpoint, off_premise_endpoint, keywords, updated_at, created_at )
 VALUES ( 7, 'HEV-GLUE', 'Human Influenza Virus', 2, 7, '1.0.0', 'www.biomex.io/asset/glue/hev/v100', 'www.offpremise.com/', 'glue algorithm DNA RNA virus human flu influenza', '2017-12-06T01:00:00.000', '2017-12-06T01:00:00.000');

INSERT INTO asset ( id, name, description, participant_id, contract_id, version, biomex_endpoint, off_premise_endpoint, keywords, updated_at, created_at )
 VALUES ( 8, 'HCV-GLUE', 'Hepatitis C Virus', 2, 8, '1.0.0', 'www.biomex.io/asset/glue/hcv/v100', 'www.offpremise.com/', 'glue algorithm DNA RNA virus human hepatitis', '2017-12-06T01:00:00.000', '2017-12-06T01:00:00.000');

/* Group data */
INSERT INTO groups ( id, name, description, updated_at, created_at ) VALUES
 ( 1, 'BiOMEX Default', 'Default group for new users', '2017-12-06T01:00:00.000', '2017-12-06T01:00:00.000');

INSERT INTO groups ( id, name, description, updated_at, created_at ) VALUES
 ( 2, 'BiOMEX Admin', 'Platform Administrators', '2017-12-06T01:00:00.000', '2017-12-06T01:00:00.000');

INSERT INTO groups ( id, name, description, updated_at, created_at ) VALUES
 ( 3, 'Asset Manager', 'Digital Biomedical Asset Manager', '2017-12-06T01:00:00.000', '2017-12-06T01:00:00.000');

INSERT INTO groups ( id, name, description, updated_at, created_at ) VALUES
 ( 4, 'Read Only', 'Read Only User', '2017-12-06T01:00:00.000', '2017-12-06T01:00:00.000');

/* User Groups Data */
INSERT INTO users_groups (user_id, group_id) values (1,2);

INSERT INTO users_groups (user_id, group_id) values (2,3);

INSERT INTO users_groups (user_id, group_id) values (3,3);

INSERT INTO users_groups (user_id, group_id) values (4,3);

INSERT INTO users_groups (user_id, group_id) values (5,3);

INSERT INTO users_groups (user_id, group_id) values (6,3);

INSERT INTO users_groups (user_id, group_id) values (7,3);

INSERT INTO users_groups (user_id, group_id) values (8,3);

INSERT INTO users_groups (user_id, group_id) values (9,3);

/* Subscription */
INSERT INTO subscription ( id, contract_id, cost, currency, term, start_date, expiry_date, created_at, updated_at) VALUES
 (1, 1111, 11.11, 'GBP', '1 YEAR', '2017-12-06T08:00:00.000', '2027-12-06T08:00:00.000', '2017-12-06T08:00:00.000', '2017-12-06T08:00:00.000');

INSERT INTO subscription ( id, contract_id, cost, currency, term, start_date, expiry_date, created_at, updated_at) VALUES
 (2, 2222, 22.22, 'USD', '2 YEARS', '2017-12-06T08:00:00.000', '2027-12-06T08:00:00.000', '2017-12-06T08:00:00.000', '2017-12-06T08:00:00.000');
