package io.biomex.biomex

import io.biomex.biomex.model.UserEntity
import io.biomex.biomex.repository.UserRepository
import org.springframework.boot.autoconfigure.domain.EntityScan
import org.springframework.context.annotation.Configuration
import org.springframework.data.jpa.repository.config.EnableJpaRepositories
import org.springframework.transaction.annotation.EnableTransactionManagement

@Configuration
@EnableJpaRepositories(basePackageClasses = [UserRepository::class])
@EntityScan(basePackageClasses = [UserEntity::class])
@EnableTransactionManagement
internal class InternalUserConfig