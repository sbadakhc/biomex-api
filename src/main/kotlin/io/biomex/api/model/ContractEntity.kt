package io.biomex.biomex.model

import io.biomex.biomex.service.dto.ContractDto
import io.biomex.biomex.service.dto.CreateContractDto
import io.biomex.biomex.service.dto.UpdateContractDto
import java.time.LocalDateTime
import javax.persistence.*

@Entity
@Table(name = "contract")
internal data class ContractEntity(

    @GeneratedValue(strategy = GenerationType.AUTO)
    @Id val id: Long? = null,
    val type: String,
    val price: String? = null,
    @JoinColumn
    val participant_id: Long? = null,
    @JoinColumn
    val asset_id: Long? = null,
    val updatedAt: LocalDateTime = LocalDateTime.now(),
    val createdAt: LocalDateTime = LocalDateTime.now()
) {

    @Suppress("unused")
    private constructor() : this(
        type = "",
        updatedAt = LocalDateTime.MIN
    )

    fun toDto(): ContractDto = ContractDto(
        id = this.id!!,
        type = this.type,
        price = this.price,
        participant_id = this.participant_id!!,
        asset_id = this.asset_id!!,
        updatedAt = this.updatedAt,
        createdAt = this.createdAt
    )

    companion object {

        fun fromDto(dto: ContractDto) = ContractEntity(
            id = dto.id,
            type = dto.type,
            price = dto.price,
            participant_id = dto.participant_id,
            asset_id = dto.asset_id,
            updatedAt = dto.updatedAt,
            createdAt = dto.createdAt
        )

        fun fromDto(dto: CreateContractDto) = ContractEntity(
            type = dto.type,
            price = dto.price,
            participant_id = dto.participant_id,
            asset_id = dto.asset_id
        )

        fun fromDto(dto: UpdateContractDto, defaultContract: ContractEntity) = ContractEntity(
            id = defaultContract.id!!,
            type = dto.type,
            price = dto.price ?: defaultContract.price,
            participant_id = dto.participant_id,
            asset_id = dto.asset_id,
            updatedAt = LocalDateTime.now(),
            createdAt = defaultContract.createdAt
        )
    }
}