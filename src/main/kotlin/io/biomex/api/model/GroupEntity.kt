package io.biomex.biomex.model

import io.biomex.biomex.service.dto.CreateGroupDto
import io.biomex.biomex.service.dto.GroupDto
import io.biomex.biomex.service.dto.UpdateGroupDto
import java.time.LocalDateTime
import javax.persistence.*

@Entity
@Table(name = "Groups")
internal data class GroupEntity(
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Id
    val id: Long? = null,
    val name: String,
    val description: String? = null,
    @ManyToMany(mappedBy = "groups")
    var users: List<UserEntity> = mutableListOf<UserEntity>(),
    val updatedAt: LocalDateTime = LocalDateTime.now(),
    val createdAt: LocalDateTime = LocalDateTime.now()
) {

    @Suppress("unused")
    private constructor() : this(
        name = "",
        updatedAt = LocalDateTime.MIN
    )

    fun toDto(): GroupDto = GroupDto(
        id = this.id!!,
        name = this.name,
        description = this.description,
        updatedAt = this.updatedAt,
        createdAt = this.createdAt
    )

    companion object {

        fun fromDto(dto: GroupDto) = GroupEntity(
            id = dto.id,
            name = dto.name,
            description = dto.description,
            updatedAt = dto.updatedAt,
            createdAt = dto.createdAt
        )

        fun fromDto(dto: CreateGroupDto) = GroupEntity(
            name = dto.name,
            description = dto.description
        )

        fun fromDto(dto: UpdateGroupDto, defaultGroup: GroupEntity) = GroupEntity(
            id = defaultGroup.id!!,
            name = dto.name,
            description = dto.description ?: defaultGroup.description,
            updatedAt = LocalDateTime.now(),
            createdAt = defaultGroup.createdAt
        )
    }
}
