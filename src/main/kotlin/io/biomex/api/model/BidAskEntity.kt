package io.biomex.biomex.model

import io.biomex.biomex.service.dto.BidAskDto
import io.biomex.biomex.service.dto.CreateBidAskDto
import io.biomex.biomex.service.dto.UpdateBidAskDto
import java.time.LocalDateTime
import javax.persistence.*

@Entity
@Table(name = "bidask")
internal data class BidAskEntity(
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Id val id: Long? = null,
    @Column(columnDefinition = "smallint")
    val contractId: Long,
    val cost: Float,
    val currency: String,
    val bid: Float,
    val ask: Float,
    val startDate: LocalDateTime = LocalDateTime.now(),
    val expiryDate: LocalDateTime = LocalDateTime.now(),
    val updatedAt: LocalDateTime = LocalDateTime.now(),
    val createdAt: LocalDateTime = LocalDateTime.now()
) {

    @Suppress("unused")
    private constructor() : this(
        contractId = 0,
        cost = 0F,
        currency = "",
        bid = 0F,
        ask = 0F,
        startDate = LocalDateTime.MIN,
        expiryDate = LocalDateTime.MIN,
        createdAt = LocalDateTime.MIN,
        updatedAt = LocalDateTime.MIN
    )

    fun toDto(): BidAskDto = BidAskDto(
        id = this.id!!,
        contractId = this.contractId,
        cost = this.cost,
        currency = this.currency,
        bid = this.bid,
        ask = this.ask,
        startDate = this.startDate,
        expiryDate = this.expiryDate,
        updatedAt = this.updatedAt,
        createdAt = this.createdAt
    )

    companion object {

        fun fromDto(dto: BidAskDto) = BidAskEntity(
            id = dto.id,
            contractId = dto.contractId,
            cost = dto.cost,
            currency = dto.currency,
            bid = dto.bid,
            ask = dto.ask,
            startDate = dto.startDate,
            expiryDate = dto.expiryDate,
            updatedAt = dto.updatedAt,
            createdAt = dto.createdAt
        )

        fun fromDto(dto: CreateBidAskDto) = BidAskEntity(
            contractId = dto.contractId,
            cost = dto.cost,
            currency = dto.currency,
            bid = dto.bid,
            ask = dto.ask,
            startDate = dto.startDate,
            expiryDate = dto.expiryDate,
            updatedAt = dto.updatedAt,
            createdAt = dto.createdAt
        )

        fun fromDto(dto: UpdateBidAskDto, defaultBidAsk: BidAskEntity) = BidAskEntity(
            id = defaultBidAsk.id!!,
            contractId = dto.contractId,
            cost = dto.cost,
            currency = dto.currency,
            bid = dto.bid,
            ask = dto.ask,
            startDate = dto.startDate,
            expiryDate = dto.expiryDate,
            updatedAt = LocalDateTime.now(),
            createdAt = defaultBidAsk.createdAt
        )
    }
}