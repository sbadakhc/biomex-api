package io.biomex.biomex.model

enum class ParticipantType {
    COMMUNITY_INTEREST_COMPANY,
    CHARITABLE_INCORPORATED_ORGANISATION,
    CO_OPERATIVE,
    GENERAL_PARTNERSHIP,
    LIMITED_LIABILITY_PARTNERSHIP,
    LIMITED_PARTNERSHIP,
    PRIVATE_LIMITED_COMPANY,
    PUBLIC_LIMITED_COMPANY,
    UNLIMITED_COMPANY,
    SOLE_PROPRIETORSHIP,
    UNIVERSITY,
    PUBLIC_RESEARCH_UNIVERSITY,
    PRIVATE_RESEARCH_UNIVERSITY
}