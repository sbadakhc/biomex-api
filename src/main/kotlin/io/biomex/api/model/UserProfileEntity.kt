package io.biomex.biomex.model

import io.biomex.biomex.service.dto.CreateUserProfileDto
import io.biomex.biomex.service.dto.UpdateUserProfileDto
import io.biomex.biomex.service.dto.UserProfileDto
import java.time.LocalDateTime
import javax.persistence.*


@Entity
@Table(name = "User_Profile")
internal data class UserProfileEntity(
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Id val id: Long? = null,
    var firstName: String,
    var lastName: String,
    var otherName: String,
    var nationality: String,
    val updatedAt: LocalDateTime = LocalDateTime.now(),
    val createdAt: LocalDateTime = LocalDateTime.now(),

    @OneToOne(mappedBy = "userProfile")
    val user: UserEntity ?= null
) {

    @Suppress("unused")
    private constructor() : this(
        firstName = "",
        lastName = "",
        otherName = "",
        nationality = "",
        updatedAt = LocalDateTime.MIN
    )

    fun toDto(): UserProfileDto = UserProfileDto(
        id = this.id!!,
        firstName = this.firstName,
        lastName = this.lastName,
        otherName = this.otherName,
        nationality = this.nationality,
        updatedAt = this.updatedAt,
        createdAt = this.createdAt
    )

    companion object {

        fun fromDto(dto: UserProfileDto) = UserProfileEntity(
            id = dto.id,
            firstName = dto.firstName,
            lastName = dto.lastName,
            otherName = dto.otherName,
            nationality = dto.nationality,
            updatedAt = dto.updatedAt,
            createdAt = dto.createdAt
        )

        fun fromDto(dto: CreateUserProfileDto) = UserProfileEntity(
            firstName = dto.firstName,
            lastName = dto.lastName,
            otherName = dto.otherName,
            nationality = dto.nationality,
            updatedAt = dto.updatedAt,
            createdAt = dto.createdAt
        )

        fun fromDto(dto: UpdateUserProfileDto, defaultUserProfile: UserProfileEntity) = UserProfileEntity(
            id = defaultUserProfile.id!!,
            firstName = dto.firstName,
            lastName = dto.lastName,
            otherName = dto.otherName,
            nationality = dto.nationality,
            updatedAt = LocalDateTime.now(),
            createdAt = defaultUserProfile.createdAt
        )
    }
}