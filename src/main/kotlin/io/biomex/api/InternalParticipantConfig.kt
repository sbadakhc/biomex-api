package io.biomex.biomex

import io.biomex.biomex.model.ParticipantEntity
import io.biomex.biomex.repository.ParticipantRepository
import org.springframework.boot.autoconfigure.domain.EntityScan
import org.springframework.context.annotation.Configuration
import org.springframework.data.jpa.repository.config.EnableJpaRepositories
import org.springframework.transaction.annotation.EnableTransactionManagement

@Configuration
@EnableJpaRepositories(basePackageClasses = arrayOf(ParticipantRepository::class))
@EntityScan(basePackageClasses = arrayOf(ParticipantEntity::class))
@EnableTransactionManagement
internal class InternalParticipantConfig
