package io.biomex.biomex.repository

import io.biomex.biomex.model.SubscriptionEntity
import org.springframework.data.jpa.repository.JpaRepository

internal interface SubscriptionRepository : JpaRepository<SubscriptionEntity, Long>
