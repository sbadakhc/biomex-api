package io.biomex.biomex.repository

import io.biomex.biomex.model.ParticipantEntity
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import javax.transaction.Transactional

@Repository
@Transactional(Transactional.TxType.MANDATORY)
internal interface ParticipantRepository : JpaRepository<ParticipantEntity, Long>