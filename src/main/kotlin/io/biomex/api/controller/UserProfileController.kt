package io.biomex.biomex.controller

import io.biomex.biomex.repository.UserProfileRepository
import io.biomex.biomex.service.UserProfileService
import io.biomex.biomex.service.dto.*
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.web.bind.annotation.*

@CrossOrigin
@RestController
@RequestMapping("/v1/userprofile")
class UserProfileController {

    @Autowired
    @Qualifier("userProfileService")
    lateinit var service: UserProfileService
    @Autowired
    internal lateinit var repository: UserProfileRepository

    private val log = LoggerFactory.getLogger(UserProfileController::class.java)

    // Route to findAllUserProfiles
    @GetMapping("/")
    fun findAllUserProfiles(): List<UserProfileDto> {
        log.debug("FindingAllUserProfiles")
        return service.findAllUserProfiles()
    }

    // Route to findByUserProfileId
    @GetMapping("/{id}")
    fun findByUserProfileId(@PathVariable("id") id: Long): UserProfileDto? {
        log.debug("FindingByUserProfileId=" + id)
        return service.findByUserProfileId(id)
    }

    @PostMapping("/")
    fun addUserProfile(@RequestBody createUserProfileDto: CreateUserProfileDto): UserProfileDto {
        log.debug("Adding UserProfile id=" + createUserProfileDto.toString())
        return service.addUserProfile(createUserProfileDto)
    }

    @PutMapping("/{id}")
    fun updateUserProfile(@RequestBody updateUserProfileDto: UpdateUserProfileDto, @PathVariable("id") id: Long): UserProfileDto? {
        log.debug("Updating UserProfile id=" + id)
        return service.updateUserProfile(id, updateUserProfileDto)
    }

    @DeleteMapping("/{id}")
    fun deleteUserProfileId(@PathVariable("id") id: Long) {
        log.debug("Deleting UserProfileId=" + id)
        return service.deleteUserProfile(id)
    }
}