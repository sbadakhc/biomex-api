package io.biomex.biomex.controller

import io.biomex.biomex.repository.ParticipantRepository
import io.biomex.biomex.service.ParticipantService
import io.biomex.biomex.service.dto.CreateParticipantDto
import io.biomex.biomex.service.dto.ParticipantDto
import io.biomex.biomex.service.dto.UpdateParticipantDto
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.web.bind.annotation.*

@CrossOrigin
@RestController
@RequestMapping("/v1/participant")
class ParticipantController {

    @Autowired
    @Qualifier("participantService")
    lateinit var service: ParticipantService
    @Autowired
    internal lateinit var repository: ParticipantRepository

    private val log = LoggerFactory.getLogger(AccountController::class.java)

    // Route to findAllParticipants
    @GetMapping("/")
    fun findAllParticipants(): List<ParticipantDto> {
        log.debug("Finding All Participants")
        return service.findAllParticipants()
    }

    // Route to findByParticipantId
    @GetMapping("/{id}")
    fun findByParticipantId(@PathVariable("id") id: Long): ParticipantDto? {
        log.debug("Finding By Participant id=" + id)
        return service.findByParticipantId(id)
    }

    @PostMapping("/")
    fun addParticipant(@RequestBody createParticipantDto: CreateParticipantDto): ParticipantDto {
        log.debug("Adding Participant id=" + createParticipantDto.toString())
        return service.addParticipant(createParticipantDto)
    }

    @PutMapping("/{id}")
    fun updateParticipant(@RequestBody updateParticipantDto: UpdateParticipantDto, @PathVariable("id") id: Long): ParticipantDto? {
        log.debug("Updating Participant id=" + id)
        return service.updateParticipant(id, updateParticipantDto)
    }

    @DeleteMapping("/{id}")
    fun deleteParticipantId(@PathVariable("id") id: Long) {
        log.debug("Deleting Participant id=" + id)
        return service.deleteParticipant(id)
    }
}
