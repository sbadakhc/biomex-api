package io.biomex.biomex.service

import io.biomex.biomex.InternalUserProfileConfig
import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Configuration

@Configuration
@ComponentScan(basePackageClasses = [InternalUserProfileConfig::class])
class UserProfileConfig
