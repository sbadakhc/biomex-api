package io.biomex.biomex.service

import io.biomex.biomex.service.dto.*

interface UserProfileService {

    fun findByUserProfileId(userProfileId: Long): UserProfileDto?

    fun findAllUserProfiles(): List<UserProfileDto>

    fun addUserProfile(userProfile: CreateUserProfileDto): UserProfileDto

    fun updateUserProfile(id: Long?, user: UpdateUserProfileDto): UserProfileDto?

    fun deleteUserProfile(id: Long?)

}