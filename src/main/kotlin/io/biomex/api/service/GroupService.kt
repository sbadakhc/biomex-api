package io.biomex.biomex.service

import io.biomex.biomex.service.dto.CreateGroupDto
import io.biomex.biomex.service.dto.GroupDto
import io.biomex.biomex.service.dto.UpdateGroupDto

interface GroupService {

    fun findByGroupId(groupId: Long): GroupDto?

    fun findAllGroups(): List<GroupDto>

    fun addGroup(group: CreateGroupDto): GroupDto

    fun updateGroup(id: Long?, group: UpdateGroupDto): GroupDto?

    fun deleteGroup(id: Long?)

}
