package io.biomex.biomex.service.dto

import org.hibernate.validator.constraints.NotEmpty
import java.time.LocalDateTime

data class CreateUserDto(
    @NotEmpty var name: String,
    var password: String? = null,
    var participant_id: Long,
    var updatedAt: LocalDateTime = LocalDateTime.now(),
    var createdAt: LocalDateTime = LocalDateTime.now()
)