package io.biomex.biomex.service.dto

import io.biomex.biomex.model.AddressType
import java.time.LocalDateTime

data class UpdateAddressDto(
    var line1: String,
    var line2: String?,
    var line3: String?,
    var city: String,
    var state: String,
    var country: String,
    var postcode: String,
    val participant_id: Long?,
    var type: AddressType,
    var updatedAt: LocalDateTime = LocalDateTime.now()
)