package io.biomex.biomex.service.dto

import org.hibernate.validator.constraints.NotEmpty
import java.time.LocalDateTime

data class CreateContractDto(
    @NotEmpty var type: String,
    var price: String? = null,
    @NotEmpty var participant_id: Long,
    @NotEmpty var asset_id: Long,
    var updatedAt: LocalDateTime = LocalDateTime.now(),
    var createdAt: LocalDateTime = LocalDateTime.now()
)