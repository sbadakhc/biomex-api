package io.biomex.biomex.service.dto

import io.biomex.biomex.model.AddressType
import java.time.LocalDateTime

data class AddressDto(
    var id: Long,
    var line1: String,
    var line2: String? = null,
    var line3: String? = null,
    var city: String,
    var state: String,
    var country: String,
    var postcode: String,
    var participant_id: Long,
    var type: AddressType,
    var updatedAt: LocalDateTime,
    var createdAt: LocalDateTime
)