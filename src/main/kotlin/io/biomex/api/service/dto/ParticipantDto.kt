package io.biomex.biomex.service.dto

import io.biomex.biomex.model.ParticipantType
import java.time.LocalDateTime

data class ParticipantDto(
    var id: Long,
    var name: String,
    var description: String? = null,
    var assets: List<AssetDto>,
    var contracts: List<ContractDto>,
    var addresses: List<AddressDto>,
    var type: ParticipantType,
    var accounts: List<AccountDto>,
    var users: List<UserDto>,
    var updatedAt: LocalDateTime,
    var createdAt: LocalDateTime
)
