package io.biomex.biomex.service.dto

import java.time.LocalDateTime

data class UpdateGroupDto(
    val name: String,
    val description: String?,
    var updatedAt: LocalDateTime = LocalDateTime.now()
)