package io.biomex.biomex.service.dto

import io.biomex.biomex.model.ParticipantType
import org.hibernate.validator.constraints.NotEmpty
import java.time.LocalDateTime

data class CreateParticipantDto(
    @NotEmpty var name: String,
    var description: String? = null,
    var type: ParticipantType,
    var updatedAt: LocalDateTime = LocalDateTime.now(),
    var createdAt: LocalDateTime = LocalDateTime.now()
)
