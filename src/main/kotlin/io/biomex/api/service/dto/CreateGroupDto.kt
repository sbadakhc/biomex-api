package io.biomex.biomex.service.dto

import org.hibernate.validator.constraints.NotEmpty
import java.time.LocalDateTime

data class CreateGroupDto(
    @NotEmpty var name: String,
    var description: String? = null,
    var updatedAt: LocalDateTime = LocalDateTime.now(),
    var createdAt: LocalDateTime = LocalDateTime.now()
)