package io.biomex.biomex.service.dto

import java.time.LocalDateTime

data class GroupDto(
    var id: Long,
    var name: String,
    var description: String? = null,
    var updatedAt: LocalDateTime,
    var createdAt: LocalDateTime
)