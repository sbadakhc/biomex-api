package io.biomex.biomex.service.dto

import java.time.LocalDateTime

data class ContractDto(
    var id: Long,
    var type: String,
    var price: String? = null,
    var participant_id: Long,
    var asset_id: Long,
    var updatedAt: LocalDateTime,
    var createdAt: LocalDateTime
)
