package io.biomex.biomex.service

import io.biomex.biomex.InternalContractConfig
import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Configuration

@Configuration
@ComponentScan(basePackageClasses = [InternalContractConfig::class])
class ContractConfig

