package io.biomex.biomex.service

import io.biomex.biomex.controller.AccountController
import io.biomex.biomex.model.SubscriptionEntity
import io.biomex.biomex.repository.SubscriptionRepository
import io.biomex.biomex.service.dto.CreateSubscriptionDto
import io.biomex.biomex.service.dto.SubscriptionDto
import io.biomex.biomex.service.dto.UpdateSubscriptionDto
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service
import javax.transaction.Transactional

@Service()
@Transactional
internal class JpaSubscriptionService(val subscriptionRepo: SubscriptionRepository) : SubscriptionService {

    private val log = LoggerFactory.getLogger(AccountController::class.java)

    override fun findAllSubscriptions(): List<SubscriptionDto> {
        return subscriptionRepo.findAll().map { it.toDto() }
    }

    override fun findBySubscriptionId(id: Long): SubscriptionDto? {
        return subscriptionRepo.findOne(id)?.toDto()
    }

    override fun addSubscription(subscription: CreateSubscriptionDto): SubscriptionDto {
        return subscriptionRepo.save(SubscriptionEntity.fromDto(subscription)).toDto()
    }

    override fun updateSubscription(subscription: UpdateSubscriptionDto, id: Long): SubscriptionDto? {
        val currentSubscription = subscriptionRepo.findOne(id)
        return if (currentSubscription != null
                       ) subscriptionRepo.save(SubscriptionEntity.fromDto(subscription, currentSubscription)).toDto()
        else null
    }

    override fun deleteSubscription(id: Long) {
        subscriptionRepo.delete(id)
    }
}