package io.biomex.biomex.service

import io.biomex.biomex.service.dto.ContractDto
import io.biomex.biomex.service.dto.CreateContractDto
import io.biomex.biomex.service.dto.UpdateContractDto

interface ContractService {

    fun findByContractId(contractId: Long): ContractDto?

    fun findAllContracts(): List<ContractDto>

    fun addContract(contract: CreateContractDto): ContractDto

    fun updateContract(id: Long?, contract: UpdateContractDto): ContractDto?

    fun deleteContract(id: Long?)

}
