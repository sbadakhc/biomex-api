package io.biomex.biomex.service

import io.biomex.biomex.model.GroupEntity
import io.biomex.biomex.repository.GroupRepository
import io.biomex.biomex.service.dto.CreateGroupDto
import io.biomex.biomex.service.dto.GroupDto
import io.biomex.biomex.service.dto.UpdateGroupDto
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service
import javax.transaction.Transactional

@Service("groupService")
@Transactional
internal class JpaGroupService(val groupRepo: GroupRepository) : GroupService {

    val log = LoggerFactory.getLogger("groupService")

    override fun findByGroupId(groupId: Long): GroupDto? {
        log.debug("Retrieving Group: {}", groupId)
        return groupRepo.findOne(groupId)?.toDto()
    }

    override fun findAllGroups(): List<GroupDto> {
        log.debug("Retrieving Groups")
        return groupRepo.findAll().map { it.toDto() }
    }

    override fun updateGroup(id: Long?, group: UpdateGroupDto): GroupDto? {
        log.debug("Updating Group: {} with data: {}", id, group)
        val currentGroup = groupRepo.findOne(id)
        return if (currentGroup != null
        ) groupRepo.save(GroupEntity.fromDto(group, currentGroup)).toDto()
        else null
    }

    override fun addGroup(group: CreateGroupDto): GroupDto {
        log.debug("Adding Group: {}", group)
        return groupRepo.save(GroupEntity.fromDto(group)).toDto()
    }

    override fun deleteGroup(id: Long?) {
        log.debug("Deleting Group: {}", id)
        groupRepo.delete(id)
    }
}

