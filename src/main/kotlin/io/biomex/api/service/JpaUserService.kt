package io.biomex.biomex.service

import io.biomex.biomex.model.UserEntity
import io.biomex.biomex.repository.UserRepository
import io.biomex.biomex.service.dto.CreateUserDto
import io.biomex.biomex.service.dto.UpdateUserDto
import io.biomex.biomex.service.dto.UserDto
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service
import javax.transaction.Transactional

@Service("userService")
@Transactional
internal class JpaUserService(val userRepository: UserRepository) : UserService {

    val log = LoggerFactory.getLogger("UserService")

    override fun findByUserId(userId: Long): UserDto? {
        log.debug(" User: {}", userId)
        return userRepository.findOne(userId)?.toDto()
    }

    override fun findAllUsers(): List<UserDto> {
        log.debug("Retrieving Users")
        return userRepository.findAll().map { it.toDto() }
    }

    override fun updateUser(id: Long?, user: UpdateUserDto): UserDto? {
        log.debug("Updating User: {} with data: {}", id, user)
        val currentUser = userRepository.findOne(id)
        return if (currentUser != null
        ) userRepository.save(UserEntity.fromDto(user, currentUser)).toDto()
        else null
    }

    override fun addUser(user: CreateUserDto): UserDto {
        log.debug("Adding User: {}", user)
        return userRepository.save(UserEntity.fromDto(user)).toDto()
    }

    override fun deleteUser(id: Long?) {
        log.debug("Deleting User: {}", id)
        userRepository.delete(id)
    }
}