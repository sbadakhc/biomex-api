package io.biomex.biomex

import io.biomex.biomex.model.ContractEntity
import io.biomex.biomex.repository.ContractRepository
import org.springframework.boot.autoconfigure.domain.EntityScan
import org.springframework.context.annotation.Configuration
import org.springframework.data.jpa.repository.config.EnableJpaRepositories
import org.springframework.transaction.annotation.EnableTransactionManagement

@Configuration
@EnableJpaRepositories(basePackageClasses = arrayOf(ContractRepository::class))
@EntityScan(basePackageClasses = arrayOf(ContractEntity::class))
@EnableTransactionManagement
internal class InternalContractConfig